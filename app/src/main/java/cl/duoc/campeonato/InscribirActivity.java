package cl.duoc.campeonato;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class InscribirActivity extends AppCompatActivity {

    private Spinner spinPosicion, spinEquipo;
    private EditText txtNombreJugador, txtRutJugador, txtFechaNacimientoJugador, txtApodoJugador, txtNroCamiseta;
    private Button btnInscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscribir);

        spinPosicion = (Spinner) findViewById(R.id.spinPosicion);
        spinEquipo = (Spinner) findViewById(R.id.spinEquipo);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, BD.getInstance().getNombreDeEquipos());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinEquipo.setAdapter(spinnerArrayAdapter);

        txtNombreJugador = (EditText) findViewById(R.id.txtNombreJugador);
        txtRutJugador = (EditText) findViewById(R.id.txtRutJugador);
        txtFechaNacimientoJugador = (EditText) findViewById(R.id.txtFechaNacimientoJugador);
        txtApodoJugador = (EditText) findViewById(R.id.txtApodoJugador);
        txtNroCamiseta = (EditText) findViewById(R.id.txtNroCamiseta);

        btnInscribir = (Button) findViewById(R.id.btnInscribir);

        btnInscribir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inscribirJugador();
            }
        });


    }

    private void inscribirJugador() {

        boolean camposCompletos = true;
        if(txtNombreJugador.getText().toString().length()<1){
            txtNombreJugador.setError("Ingrese el nombre del jugador");
            camposCompletos = false;
        }if(txtRutJugador.getText().toString().length()<1){
            txtRutJugador.setError("Ingrese el rut del jugador");
            camposCompletos = false;
        }if(txtFechaNacimientoJugador.getText().toString().length()<1){
            txtFechaNacimientoJugador.setError("Ingrese la fecha de nacimiento");
            camposCompletos = false;
        }if(txtApodoJugador.getText().toString().length()<1){
            txtApodoJugador.setError("Ingrese el apodo del jugador");
            camposCompletos = false;
        }if(txtNroCamiseta.getText().toString().length()<1){
            txtNroCamiseta.setError("Ingrese el numero de camiseta");
            camposCompletos = false;
        }if(camposCompletos){
            Jugador nuevoJugador= new Jugador(txtApodoJugador.getText().toString(),txtNombreJugador.getText().toString(),txtRutJugador.getText().toString(),Integer.parseInt(txtNroCamiseta.getText().toString()),spinPosicion.getSelectedItem().toString(),txtFechaNacimientoJugador.getText().toString());
            BD.getInstance().agregarJugador(spinEquipo.getSelectedItem().toString(),nuevoJugador);
            Toast.makeText(InscribirActivity.this, "Jugador agregado", Toast.LENGTH_SHORT).show();
        }
    }

}
