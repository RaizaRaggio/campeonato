package cl.duoc.campeonato;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rzrgg on 15/09/2016.
 */
public class EquiposAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Equipo> dataSource;

    public EquiposAdapter(Context context, ArrayList<Equipo> dataSource) {
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.equipos, parent, false);
        }

        // Set data into the view.
        TextView txtNombreEquipo=(TextView) rowView.findViewById(R.id.txtNombreEquipo);
        TextView txtJugadores=(TextView) rowView.findViewById(R.id.txtJugadores);
        TextView txtAnio=(TextView) rowView.findViewById(R.id.txtAnio);

        Equipo item = this.dataSource.get(position);
        txtNombreEquipo.setText(item.getNombre());
        txtAnio.setText(String.valueOf(item.getFecha_creacion()));

        String jug = "";
        for(Jugador jugador:item.getJugadores()) {
            jug += jugador.getNombre() + "" + jugador.getN_camiseta() + "\n";
        }

        txtJugadores.setText(jug);

        return rowView;


    }
}

