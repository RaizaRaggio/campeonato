package cl.duoc.campeonato;

import java.security.PublicKey;
import java.util.ArrayList;

/**
 * Created by rzrgg on 09/09/2016.
 */
public class BD {

    private static BD instance;
    private static ArrayList<Equipo> equipos = new ArrayList<>();

    private BD(){}

    public static BD getInstance(){
        if(instance==null){
            instance=new BD();
        }
        return instance;
    }

    public void agregarEquipo(Equipo equipo){
        equipos.add(equipo);
    }

    public Equipo buscarEquipoPorNombre(String nombre){
        for(int x = 0 ; x<equipos.size();x++){
            if(equipos.get(x).getNombre().equals(nombre)){
                return equipos.get(x);
            }
        }
        return null;
    }

    public void agregarJugador(String nombreEquipo, Jugador jugador){
        Equipo equipo = buscarEquipoPorNombre(nombreEquipo);

        if(equipo!=null){
            equipo.agregarJugador(jugador);
        }
    }

    public ArrayList<String> getNombreDeEquipos(){
        ArrayList<String> nombres = new ArrayList<>();
        for(Equipo aux : equipos){
            nombres.add(aux.getNombre());
        }
        return nombres;
    }

    public ArrayList<Equipo> listaEquipos(){
        return equipos;
    }
}

