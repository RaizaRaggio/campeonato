package cl.duoc.campeonato;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ListaActivity extends AppCompatActivity {

    private ListView lvListaEquipos;
    private EquiposAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        lvListaEquipos = (ListView)findViewById(R.id.lvListaEquipos);
        adapter = new EquiposAdapter(this, BD.getInstance().listaEquipos());
        lvListaEquipos.setAdapter(adapter);
    }
}
