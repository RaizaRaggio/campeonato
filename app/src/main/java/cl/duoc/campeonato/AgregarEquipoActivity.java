package cl.duoc.campeonato;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregarEquipoActivity extends AppCompatActivity {

    private EditText txtNombreEquipo, txtFechaCreacion, txtUrlImagen, txtEntrenador, txtColorCamiseta;
    private Button btnAgregarEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_equipo);

        txtNombreEquipo = (EditText) findViewById(R.id.txtNombreEquipo);
        txtFechaCreacion = (EditText) findViewById(R.id.txtFechaCreacion);
        txtUrlImagen = (EditText) findViewById(R.id.txtUrlImagen);
        txtEntrenador = (EditText) findViewById(R.id.txtEntrenador);
        txtColorCamiseta = (EditText) findViewById(R.id.txtColorCamiseta);

        btnAgregarEquipo = (Button) findViewById(R.id.btnAgregarEquipo);

        btnAgregarEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarEquipo();
            }
        });

    }

    private void agregarEquipo() {
        boolean camposCompletos=true;
        if(txtNombreEquipo.getText().toString().length()<1){
            txtNombreEquipo.setError("Ingrese el nombre del equipo");
            camposCompletos=false;
        } if(txtFechaCreacion.getText().toString().length()<1){
            txtFechaCreacion.setError("Ingrese la fecha de creacion");
            camposCompletos=false;
        } if(txtUrlImagen.getText().toString().length()<1){
            txtUrlImagen.setError("Ingrese la url de la imagen");
            camposCompletos=false;
        } if(txtEntrenador.getText().toString().length()<1){
            txtEntrenador.setError("Ingrese el nombre del entrenador");
            camposCompletos=false;
        } if(txtColorCamiseta.getText().toString().length()<1){
            txtColorCamiseta.setError("Ingrese el color de la camiseta");
            camposCompletos=false;
        } if(camposCompletos){
            Equipo nuevoEquipo = new Equipo(txtNombreEquipo.getText().toString(),txtUrlImagen.getText().toString(),txtFechaCreacion.getText().toString(),txtColorCamiseta.getText().toString(),txtEntrenador.getText().toString(),"1");
            BD.getInstance().agregarEquipo(nuevoEquipo);
            Toast.makeText(AgregarEquipoActivity.this, "Equipo agregado", Toast.LENGTH_SHORT).show();
        }
    }
}
