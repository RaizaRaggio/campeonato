package cl.duoc.campeonato;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    private Button btnInscribirJugador, btnCrearEquipo, btnListarEquipo;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnCrearEquipo = (Button) findViewById(R.id.btnCrearEquipo);
        btnInscribirJugador =(Button)findViewById(R.id.btnInscribirJugador);
        btnListarEquipo = (Button) findViewById(R.id.btnListarEquipo);

        btnCrearEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this,AgregarEquipoActivity.class);
                startActivity(i);
            }
        });

        btnInscribirJugador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MenuActivity.this,InscribirActivity.class);
                startActivity(i);
            }
        });

        btnListarEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this,ListaActivity.class);
                startActivity(i);
            }
        });

    }
}
