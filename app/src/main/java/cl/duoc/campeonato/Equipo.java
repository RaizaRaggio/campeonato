
package cl.duoc.campeonato;

import java.util.ArrayList;

/**
 * Created by rzrgg on 09/09/2016.
 */
public class Equipo {

    private String id;
    private String nombre;
    private String fecha_creacion;
    private String urlInsignia;
    private String color_camiseta;
    private String entrenador;
    private ArrayList<Jugador> jugadores;

    public Equipo(String nombre, String urlInsignia, String fecha_creacion, String color_camiseta, String entrenador, String id) {
        this.nombre = nombre;
        this.urlInsignia = urlInsignia;
        this.fecha_creacion = fecha_creacion;
        this.color_camiseta = color_camiseta;
        this.entrenador = entrenador;
        this.id = id;
        jugadores= new ArrayList<>();
    }

    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(ArrayList<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlInsignia() {
        return urlInsignia;
    }

    public void setUrlInsignia(String urlInsignia) {
        this.urlInsignia = urlInsignia;
    }

    public String getColor_camiseta() {
        return color_camiseta;
    }

    public void setColor_camiseta(String color_camiseta) {
        this.color_camiseta = color_camiseta;
    }

    public String getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(String entrenador) {
        this.entrenador = entrenador;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void agregarJugador(Jugador nuevoJugador){
        jugadores.add(nuevoJugador);
    }

}
