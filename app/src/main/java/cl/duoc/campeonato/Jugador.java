package cl.duoc.campeonato;

import java.util.Date;

/**
 * Created by rzrgg on 09/09/2016.
 */
public class Jugador {

    private String rut;
    private String nombre;
    private String apodo;
    private int n_camiseta;
    private String posicion;
    private String fecha_nacimiento;

    public Jugador(String apodo, String nombre, String rut, int n_camiseta, String posicion, String fecha_nacimiento) {
        this.apodo = apodo;
        this.nombre = nombre;
        this.rut = rut;
        this.n_camiseta = n_camiseta;
        this.posicion = posicion;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getN_camiseta() {
        return n_camiseta;
    }

    public void setN_camiseta(int n_camiseta) {
        this.n_camiseta = n_camiseta;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
}
